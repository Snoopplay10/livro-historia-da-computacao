# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/primeiro_computador.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/evolucao_do_computador.md)
    - [Primeira Geração]()
1. [Computação Móvel](capitulos/computacao_movel.md)
1. [Futuro](capitulos/futuro.md)
1. [A Última Pergunta (Isaac Asimov)](capitulos/a_ultima_pergunta.md)
1. [Uma Área Diversa](capitulos/uma_area_diversa.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9267493/avatar.png?width=400)  | Yan Pablo | yanpablo | [yanpablocarvalho@gmail.com](mailto:yanpablocarvalho@gmail.com)
|![](https://gitlab.com/uploads/-/system/user/avatar/9168492/avatar.png?width=400)| Kelvyn Nonato | Snoopplay10 | [kelvyn_nonato@hotmail.com](mailto:kelvyn_nonato@hotmail.com)
|![](https://gitlab.com/uploads/-/system/user/avatar/9123716/avatar.png?width=400)| Gabriel Maurina | GabrielMaurina | [gabrielmaurina2003@gmail.com](mailto:gabrielmaurina2003@gmail.com)
|![](https://gitlab.com/uploads/-/system/user/avatar/9236931/avatar.png?width=400)| Thomas Farias |  thomasfariasg |[thomasfariasg@gmail.com](mailto:thomasfariasg@gmail.com)
